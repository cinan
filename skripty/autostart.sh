#!/bin/sh
run_once pidgin &
run_once parcellite -n &
if [ -e /var/run/daemons/rtorrent ]; then
      sakura -e "screen -r" &
fi
run_once hudba &>/dev/null &
#run_once sakura -e "/home/cinan/skripty/misc.sh" &
run_once mpdscribble --conf ~/.mpdscribble/mpdscribble.conf --log ~/.mpdscribble/mpdscribble.log --cache ~/.mpdscribble/mpdscribble.cache &
run_once /opt/mumbles/src/mumbles &
