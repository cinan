#program kazdych 60sekund kontroluje, ci nejaky torrent je na 100% stiahnuty
#ak ano, tak ho stopne
#
#Aby to fungovalo, treba v Transmission zapnut web rozhranie 
#a potom sa bude dat napojit cez transmission-remote (defaultny port je 9091)
#
#meno a heslo sa da nastavit editovanim 
#
#autor: cinan
puts "Kontrolujem stav torrentov..."
while true

#najprv zistim pocet torrentov a odrezem prvy riadok, ktory je zbytocny
obsah = `transmission-remote --list`
if obsah == []
  #prazdny, nic sa nestahuje/neuploaduje
  puts "Nic sa nestahuje."
  exit
end

i = 0
#odrezem prvy riadok:
until obsah[i].chr == "\n"
  i = i + 1
end
obsah = obsah[i+1..-1]

obsah2 = []
for i in 0...obsah.length
  if obsah[i].chr == "\n"
    obsah2[i] = obsah[0..i]
  end
end
obsah2.compact!
obsah = obsah2.clone

for i in 1..obsah.length
  vysledok = `transmission-remote -t#{i} -i | grep "Percent Done: 100%"`
  if vysledok != ""
    #stopneme torrent
    `transmission-remote -t#{i} -S`
  end
end

sleep(60)

end
