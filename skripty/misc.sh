#/bin/sh
ck='\e[0;30m'
blue='\e[0;34m'
green='\e[0;32m'
cyan='\e[0;36m'
red='\e[0;31m'
purple='\e[0;35m'
brown='\e[0;33m'
lightgray='\e[0;37m'
darkgray='\e[1;30m'
lightblue='\e[1;34m'
lightgreen='\e[1;32m'
lightcyan='\e[1;36m'
lightred='\e[1;31m'
lightpurple='\e[1;35m'
yellow='\e[1;33m'
white='\e[1;37m'
nc='\e[0m'

localnet ()
{
    echo -e "\t\t\t    IP Address: " `/sbin/ifconfig eth0 | awk '/inet addr/ {split ($2,A,":"); print A[2]}'`
    #echo ""
    #/sbin/ifconfig | awk /'Bcast/ {print $3}' 
    #echo ""
}
upinfo ()
{
    echo -ne "${darkgray} \t ";uptime | awk /'up/ {print $3,"Load average: "$8,$9,$10}'
}
while [ 1==1 ]; do
clear
#LINES=("$@") 
#let i=($LINES/3)
i=3
echo -en "\033[${i}B"
echo -e "${cyan}";figlet -d ~/.figlet/fonts Open - Source -f avatar -c;
echo -e "      ${darkgray}" `uname -smr` "\t\t\t cinan@cinan-desktop"
#upinfo
localnet
echo -e "${lightgray}"; cal -m
ruby ~/skripty/hodiny.rb;
echo -en "\033[4A\033[51C"
sleep 10
done
