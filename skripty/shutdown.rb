require 'gtk2'

# Creates the window.
window = Gtk::Window.new

# The program will directly end upon 'delete_event'.
window.signal_connect('delete_event') do
  Gtk.main_quit
  false
end

# We create a box to pack widgets into.  
# This is described in detail in the following section.
# The box is not really visible, it is just used as a tool to arrange 
# widgets.
box1 = Gtk::HBox.new(true, 0)

# Put the box into the main window.
window.add(box1)

# Creates a new button with the label "Button 1".
button3 = Gtk::Button.new("Shut down")
button1 = Gtk::Button.new("Restart")
button2 = Gtk::Button.new("Suspend")
button4 = Gtk::Button.new("Lock screen")

# Now when the button is clicked, we call the "callback" method
# with a reference to "button 1" as its argument.
button2.signal_connect( "clicked" ) do 
  `sudo reboot`
end

button3.signal_connect("clicked") do
  `sudo pm-suspend`
  `/etc/rc.d/mpd stop`
end

button1.signal_connect("clicked") do
  `sudo shutdown -h now`
end

button4.signal_connect("clicked") do 
  `xlock -mode blank`
end
# Instead of window.add, we pack this button into the invisible
# box, which has been packed into the window.
box1.pack_start(button3, true, true, 0)
box1.pack_start(button1, true, true, 0)
box1.pack_start(button2, true, true, 0)
box1.pack_start(button4, true, true, 0)

window.show_all
Gtk.main
