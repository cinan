#1.4.7 Ruby verzia
#autor: cinan
#pri hladani vypise dvakrat to iste                       FIXED
class Trieda
  @@path_zoznam_lst = `echo $HOME`.chomp+"/zoznam_real.lst"
  @@farbaNula = "\x1B[0m"
  @@farbaModra = "\033[94m"
  @@farbaCervena= "\033[91m"
  @@farbaZelena = "\033[92m"
  @@podmienka_na_ukoncenie = false

  def moznostiSkopirovat?
    puts "\t-"+"="*51+"-\n"+"\t"+"#{@@farbaZelena}0#{@@farbaNula}"+". Spat" + " "*21 +"Skopirovat prikaz c. "+"#{@@farbaZelena}[1-#{@@i-1}]#{@@farbaNula}"
    print "\t"+"Volba c. #{@@farbaZelena}0\b"
    cislo = gets.chomp
    puts @@farbaNula
    if cislo.to_i >= 1
      begin
        system("echo '#{@@zoznam[cislo.to_i-1][1]}' | xsel --clipboard")
      rescue
        puts @@farbaCervena + "Zadal si nespravnu hodnotu!" + @@farbaNula
        moznostiSkopirovat?
      end
    end
  end
  
  def zobrazit_zoznam
    if File.exist?(@@path_zoznam_lst)
      subor = File.open(@@path_zoznam_lst,"r")     #vypis zoznamu
      @@i = 1
      @@zoznam=[]
      for riadok in subor
        riadok = riadok.split(".:.")
        print @@farbaZelena + @@i.to_s + @@farbaNula + ".\t" + @@farbaModra + riadok[0] + @@farbaNula + "\n\t" + riadok[1].to_s
        @@zoznam.push(riadok)
        @@zoznam[@@i-1][1] = @@zoznam[@@i-1][1][0...-1]
        @@i = @@i + 1
      end
      subor.close
      moznostiSkopirovat?
    else
      puts @@farbaCervena + "Nie je vytvoreny zoznam!" + @@farbaNula
    end
    
    podmienka_na_ukoncenie?
    self.menu
  end
  
  def vymazat_zoznam
    if File.exist?(@@path_zoznam_lst) then
      print "Naozaj chces vymazat zoznam a vytvorit novy? [Y/n] "
      odpoved = gets.chomp
      if odpoved == "Y" or odpoved == "y" or odpoved == ""
        File.delete(@@path_zoznam_lst)
        puts "Zoznam je vymazany!"
      end
    else
      puts @@farbaCervena + "Nie je vytvoreny zoznam!" + @@farbaNula
    end
    
    podmienka_na_ukoncenie?
    gets.chomp
    self.menu
  end
   
  def vymazat_prikaz
    if File.exist?(@@path_zoznam_lst) then
      subor = File.open(@@path_zoznam_lst,"r")     #najprv nacitat zoznam zo suboru
      i = 0
      zoznam=[]
      for riadok in subor
        zoznam.push(riadok.split(".:. "))
        zoznam[i][1] = zoznam[i][1][0..-1]
        i = i + 1
      end
      subor.close
      
      print "Cisla prikazov na vymazanie oddelene ciarkou: "
      cislo = gets.chomp
      if cislo == ""
        self.menu
      end
      cislo = cislo.split(",")
      i = 0
      for i in 0...cislo.length            #integer hodnoty v cislo2
        cislo[i]=cislo[i].to_i
      end
      
      i = 0
      for f in cislo
        if f == 0
          puts @@farbaCervena + "Zadal si nespravne hodnoty:\nPrikaz c. " + f.to_s + " nemohol byt vymazany!\n" + @@farbaNula
        else
          begin
            zoznam[f-1]=""
            i = i + 1
            puts "Prikaz c. " + f.to_s + " bol vymazany!"
          rescue
            puts @@farbaCervena + "Zadal si nespravne hodnoty:\nPrikaz c. " + f.to_s + " nemohol byt vymazany!\n" + @@farbaNula
          end
        end
      end
      x = 0
      while x < i
        zoznam.delete("")
        x = x + 1
      end
      i = 0
      subor = File.open(@@path_zoznam_lst,"w")     #zapisanie do zoznamu
      while i < zoznam.length
        subor.write(zoznam[i][0]+".:. "+zoznam[i][1][0..-2]+"\n")
        i = i + 1
      end
      subor.close
    else
      puts @@farbaCervena + "Nie je vytvoreny zoznam!" + @@farbaNula
    end

    gets
    self.menu
  end
  
  def hladat
    if File.exists?(@@path_zoznam_lst)
      subor = File.open(@@path_zoznam_lst,"r")     #najprv nacitat zoznam zo suboru
      i = 0
      zoznam=[]
      for riadok in subor
        zoznam.push(riadok.split(".:. "))
        zoznam[i][1] = zoznam[i][1][0...-1]
        i = i + 1
      end
      subor.close
    else
      puts @@farbaCervena + "Nie je vytvoreny zoznam!" + @@farbaNula
      podmienka_na_ukoncenie?
      gets
      self.menu
    end
   
    i = 0                                     #nazvy v zozname hodim do malych pismen#
    zoznam_dalsi = []
    zoznam_pri_vypise = zoznam.clone                #pri vysledkoch hladania vypisem originalnou velkostou
    while i < zoznam.length
      prve_slovo = zoznam[i][0].downcase
      druhe_slovo = zoznam[i][1].downcase
      zoznam_dalsi.push([prve_slovo,druhe_slovo])
      i = i + 1
    end
    zoznam = zoznam_dalsi # :)

    begin 
      hladaj = @@hladaj.downcase #ak sme zadali hladaj cez parameter pred tym
    rescue
      print "Co chces hladat: "
      hladaj = gets.chomp
      if hladaj == ""
        self.menu
      end
      hladaj = hladaj.downcase 
    end
    
    vypis = []
    a = 0
    zoznam_poradie = []
    for slovo1,slovo2 in zoznam
      if /#{hladaj}/ =~ slovo1[0..-1] or /#{hladaj}/ =~ slovo2[0..-1]
        #if not zoznam_pri_vypise[a][0] == vypis[-1]
          vypis.push(zoznam_pri_vypise[a])
          zoznam_poradie.push(a+1)
        #end
      end
      a = a + 1
    end
    
    puts "Cislo | V zozname |               Nazov / Prikaz"
    puts "-------------------------------------------------------------"
    @@i = 1
    for dve_slova in vypis
        puts @@farbaZelena + @@i.to_s + "#{@@farbaNula}.\t  #{zoznam_poradie[0]}"  + @@farbaNula + "." + @@farbaModra + "\t    " + dve_slova[0].to_s + @@farbaNula+"\n\t\t   " + dve_slova[1].to_s
        @@i = @@i + 1
        zoznam_poradie.delete_at(0)
    end
    @@zoznam = vypis.clone
    moznostiSkopirovat?
    podmienka_na_ukoncenie?
    self.menu
  end
  
  def pridat
    if File.exist?(@@path_zoznam_lst)
      subor = File.open(@@path_zoznam_lst,"r")
      puts @@farbaModra + "Pre ukoncenie pridavania stlac \"Enter\" v poli nazov" + @@farbaNula
      zoznam_original = []
                 
      i = 0
      zoznam=[]
      for riadok in subor
        zoznam.push(riadok.split(".:. "))
        zoznam[i][1] = zoznam[i][1][0..-1]
        i = i + 1
      end
      
      a = 0
      while (a == 0)
        print "Zadaj nazov: "
        meno = gets.chomp
        if meno != ""
          pis = true
          print "Napis prikaz: "
          prikaz = gets
          for prvok in zoznam
            if prvok[1] == prikaz
              puts @@farbaCervena + "Taky prikaz uz v zozname existuje a preto nebude duplikovany." + @@farbaNula
              pis = false
            end
          end
          for prvok in zoznam_original
            if prvok[1] == prikaz
              puts @@farbaCervena + "Taky prikaz uz v zozname existuje a preto nebude duplikovany." + @@farbaNula
              pis = false
            end
          end
          
          if pis == true
            zoznam_original.push([meno,prikaz])
          end
        else
          a = 1
        end
      end
      subor.close
     
      zoznam_konecny = zoznam + zoznam_original
      zoznam_konecny.sort!
      
      i = 0
      subor = File.open(@@path_zoznam_lst,"w")     #dopisanie zoznamu
      while i < zoznam_konecny.length
        subor.write(zoznam_konecny[i][0]+".:. "+zoznam_konecny[i][1][0..-2]+"\n")
        i = i + 1
      end
      subor.close
      podmienka_na_ukoncenie?
      self.menu
    else
      puts @@farbaCervena + "Nie je vytvoreny zoznam!" + @@farbaNula
      podmienka_na_ukoncenie?
      self.menu
    end
    
  end
  
  def prepisat
    print "Naozaj chces vymazat zoznam a vytvorit novy? [Y/n] "
    odpoved = gets.chomp
    if odpoved == "Y" or odpoved == "y" or odpoved == ""
      subor = File.open(@@path_zoznam_lst,"w")
      subor.close
      self.pridat
    end
    podmienka_na_ukoncenie?
    self.menu
  end
   
  def about
    logo
    puts "\t\|autor:    cinan                    verzia: 1.4.7 Ruby  \|"
    puts "\t\|" + " "*53 + "\|"
    puts "\t\|                        Naco sluzi tento program?    \|"
    puts "\t\|Autor vytvoril tento program na zaznamenavanie       \|"
    puts "\t\|prikazov v GNU/Linuxe, ktore malo pouziva.           \|"
    puts "\t\|Neskor bolo pridanych niekolko zbytocnych funkcii    \|"
    puts "\t\|pre srandu kralikov.                                 \|"
    podmienka_na_ukoncenie?
    gets
    self.menu
  end
   
  def menu
    puts "\t-"+"="*53+"-"
    puts "\t\|"+"\t\t#{@@farbaZelena}1#{@@farbaNula}"+". Zobrazit zoznam                    \|"
    puts "\t\|"+"\t\t#{@@farbaZelena}2#{@@farbaNula}"+". Hladat v zozname                   \|"
    puts "\t\|"+"\t\t#{@@farbaZelena}3#{@@farbaNula}"+". Pridat udaje                       \|"
    puts "\t\|"+"\t\t#{@@farbaZelena}4#{@@farbaNula}"+". Vymazat udaje                      \|"
    puts "\t\|"+" "*53+"\|"
    puts "\t\|"+"\t\t#{@@farbaZelena}5#{@@farbaNula}"+". Prepisat do noveho zoznamu         \|"
    puts "\t\|  " + "#{@@farbaZelena}q#{@@farbaNula}" + ". Ukoncit" + " "*27 + "#{@@farbaZelena}6#{@@farbaNula}"+". O programe \|"
    puts "\t-"+"="*53+"-"
    print "Moznost c.: "
    print @@farbaZelena
    moznost = gets.chomp.to_s
    puts @@farbaNula
   
    if moznost == "1"
      self.zobrazit_zoznam
    elsif moznost == "2"
      self.hladat
    elsif moznost == "3"
      self.pridat
    elsif moznost == "4"
      self.vymazat_prikaz
    elsif moznost == "5"
      self.prepisat
    elsif moznost == "q"
      exit
    elsif moznost == "6"
      self.about
    else
      puts @@farbaCervena + "Zadana moznost neexistuje!" + @@farbaNula
      self.menu
    end
  end
  
  def logo    
    puts "\t-" + "="*53 + "-"
    puts "\t\|#{@@farbaModra}                               _               _     #{@@farbaNula}\|"
    puts "\t\|#{@@farbaModra}  ___   ___   __ _  _ __  ___ | |__      _ __ | |__  #{@@farbaNula}\|"
    puts "\t\|#{@@farbaModra} \/ __| / _ \\ \/ _` || '__|\/ __|| '_ \\    | '__|| '_ \\ #{@@farbaNula}\|"
    puts "\t\|#{@@farbaModra} \\__ \\|  __\/| (_| || |  | (__ | | | | _ | |   | |_) |#{@@farbaNula}\|"
    puts "\t\|#{@@farbaModra} |___\/ \\___| \\__,_||_|   \\___||_| |_|(_)|_|   |_.__\/ #{@@farbaNula}\|"
  end
  
  def podmienka_na_ukoncenie?
    if @@podmienka_na_ukoncenie == true
      exit
    end
  end
  
  def initialize
    if ARGV.size > 0
      @@podmienka_na_ukoncenie = true
        case ARGV[0]
          when "--help", "-h"
            puts "Pouzitie: 
             --help, -h \t\t\t Tato pomoc
             --show-list, -l \t\t Zobrazit zoznam
             --search=SEARCH, -s=SEARCH \t Hladat 
             --search, -s \t\t Hladat
             --add, -a \t\t\t Pridat
             --create, -c \t\t Vytvorit novy zoznam
             --about   \t\t\t O programe"
            exit
        when "--show-list", "-l"
          $*.clear
          self.zobrazit_zoznam
        when /^--search=([^ ]+)/, /^-s=([^ ]+)/
          $*.clear
          @@hladaj =  $1.to_s
          self.hladat
        when "-s", "--search"
          $*.clear
          self.hladat
        when "--add", "-a"
          $*.clear
          self.pridat
        when "--about"
          $*.clear
          self.about
        when "--create", "-c"
          $*.clear
          self.prepisat
        else
          @@hladaj = ARGV.join(" ")
          $*.clear
          self.hladat
        end
    end
  end
end

praca = Trieda.new
praca.logo
praca.menu
