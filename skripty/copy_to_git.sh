#!/bin/sh
cd /home/cinan/.config
rsync -av --delete awesome Terminal openbox parcellite sakura /home/cinan/git/configs/
cd /home/cinan
rsync -av --delete .Xdefaults .bashrc .bash_profile .conkyrc .fonts.conf .gtkrc-2.0 .gtkrc-2.0-kde4 .ncmpcpp .rtorrent.rc .vimrc .vimperatorrc .xinitrc /home/cinan/git/configs/
rsync -av --delete /etc/fonts /home/cinan/git/configs/etc/ 
