# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# Colour Codes
export Cyan="\[\e[m\]\[\e[0;36m\]"
export Red="\[\e[m\]\[\e[0;31m\]"
export LightCyan="\[\e[m\]\[\e[1;36m\]"
export LightRed="\[\e[m\]\[\e[1;31m\]"
export white="\[\e[0;37m\]"


function pre_prompt
{
    newPWD="${PWD}"
    let promptsize=$(echo -n "╒═[ $newPWD ]}╗" | wc -c | tr -d " ")
    width=$COLUMNS
    let fillsize=${width}-${promptsize}-1
    let helping=$fillsize
    fill=""
    
    while [ "$fillsize" -gt "0" ]
    do
        fill="${fill}═"
        let fillsize=${fillsize}-${promptsize}
    done
    
    if [ $helping == $fillsize ]
    then
        let cutt=3-${fillsize}
        newPWD="...$(echo -n $PWD | sed -e "s/\(^.\{$cutt\}\)\(.*\)/\2/")"
    fi
}


# Set prompt colour
if [ `id -u` -eq 0 ]
then
    cText="${LightRed}"
    cBorder="${Red}"
else
    cText="${LightCyan}"
    cBorder="${Cyan}"
fi

PROMPT_COMMAND=pre_prompt

# Display Prompt
PS1="  ${cBorder}╒═${cBorder}═[ ${cText}\$newPWD${cBorder} ]\${fill}╕\n  ${cBorder}└${cBorder}> ${white}"

# To enable the settings / commands in this file for login shells as well,
# this file has to be sourced in /etc/profile.

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# set variable identifying the chroot you work in (used in the prompt below)
#if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
#    debian_chroot=$(cat /etc/debian_chroot)
#fi

# set a fancy prompt (non-color, overwrite the one in /etc/profile)
#PS1='\[\e[0;32m\]\u@\h:\w\$\[\e[1;37m\] '
#PS1='\[\e[0;36m\]\w\$\[\e[0;37m\] '
#export PROMPT_COMMAND='cwd=${PWD%/}; thisdir="$(echo $cwd | sed '\''s/.*\///'\'')"; dirparts="${cwd:0:$((${#cwd} - ${#thisdir}))}"; dirparts="$(echo ${dirparts%/} | sed '\''s/\//\\[\\e[0;33m\\]\/\\[\\e[32m\\]/g'\'')"; rc=${PWD%/}; rc=${rc:0:1}; PS1="\[\e]0;${cwd}/\007\e[1;36m\][\[\e[0;36m\]$(date "+%I:%M:%S %p")\[\e[1m\]] ${dirparts}\[\e[0;33m\]${rc}\[\e[1;34m\]${thisdir}\[\e[0;33m\]/\[\e[1;33m\] %\[\e[0m\] "'

# enable bash completion in interactive shells
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

export TERM=xterm-256color


#FUNCTIONS
#delete all files except $1, now works with directories and with rm -r
function rr () {
    if [ "$1" == "-rf" ] || [ "$1" == "-Rf" ];
    then
        a=$2; p="-rf";
    else
        a=$1; p="";
    fi
    c=$(echo $a | grep -o "/" | wc -w); 
    if [ $c -eq 0 ]; then a=./$a; c=1; fi
    cd $(echo $a | cut -d / -f 1-$c); 
    rm $p `ls -1 --color=none | grep -vxF $(echo $a | cut -d / -f $(echo $[c+=1]))`; 
    cd $OLDPWD; 
}

#find out ATI card temperature and set fan speed
function teplota { LD_PRELOAD=libXinerama.so.1 aticonfig --od-gettemperature; }
function fan() { aticonfig --pplib-cmd "set fanspeed 0 $1"; }


alias ls="ls --color -h"
#alias editt="sudo vi /usr/share/themes/openbox_cinan/openbox-3/themerc"
#alias pal="xterm -hold -bg black -fg white -geometry 37x12 -e pal"
alias pacman="pacman-color"
alias du="du -hs"
alias df="df -h"
alias grep="grep --colour -i"
alias rubysearch="ruby ~/skripty/search_interface.rb"
alias vi="vim -u ~/.vimrc"
alias more="less"
alias rtorrent="rtorrent -o http_capath=/etc/ssl/certs"
alias smplayer="smplayer 2>/dev/null"
alias f="cd /media/data/filmy"
alias F="cd /media/data"
alias d="cd /media/data2"
alias D="cd /media/data2/download"
alias "true-combat"="fan 50; true-combat.alsa; fan 3;"
alias soffice="env GTK2_RC_FILES=/usr/share/themes/Redmond/gtk-2.0/gtkrc env OOO_FORCE_DESKTOP=gnome soffice"
alias t="vim -c :TodoOpen -c :q"
