if v:progname =~? "evim"
  finish
endif

set nocompatible

set backspace=indent,eol,start

map Q gq

" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

"if has("gui_running")
"    colo darkspectrum
"else
    colo mustang
"endif
if &t_Co > 2 || has("gui_running")
  syntax on
  set hlsearch
endif

if has("autocmd")

  filetype plugin indent on
  filetype plugin on 
  augroup vimrcEx
  au!

  autocmd FileType text setlocal textwidth=78

  autocmd BufReadPost *
    \ if line("'\"") > 1 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif

  augroup END

else

  set autoindent		" always set autoindenting on

endif " has("autocmd")

if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif

set showmatch
set incsearch
set number
set history=100
set ruler
set showcmd
set hlsearch
set smartindent
set smarttab
set smartcase
set expandtab
set wildmenu
set ignorecase
set shiftwidth=4
set nowrap
set showmode
set cursorline
set background=dark
set nojoinspaces
set mouse=r
"set paste
nnoremap <F12> :TlistToggle<CR>
"fix backspace
nmap  hx
"nmap a i
"F2 = save file
map <F2> :wh 
"autocmd BufRead *.rb set foldmethod=syntax
"autocmd BufRead *.rb set foldcolumn=5
vmap <C-y> y:call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>
nmap <C-y> yw:call system("xclip -i -selection clipboard", getreg("\""))<CR>:call system("xclip -i", getreg("\""))<CR>
nmap <C-v> :call setreg("\"",system("xclip -o -selection clipboard"))<CR>p

au BufRead /tmp/mutt* set tw=72
au BufRead /tmp/mutt* :normal G?--
au BufRead /tmp/mutt* :normal k

"F5 = run program
autocmd BufRead *.rb map <F5> :!clear; ruby % 
au BufNewFile,BufRead *.srt setf srt
syntax on
if has("autocmd")
  filetype indent on
endif

augroup templates
  au!
  " read in template files
  autocmd BufNewFile *.* silent! execute '0r /home/cinan/.vim/templates/skeleton.'.expand("<afile>:e")
  " autocmd BufNewFile * %substitute#\[:VIM_EVAL:\]\(.\{-\}\)\[:END_EVAL:\]#\=eval(submatch(1))#ge
  autocmd BufNewFile *.cs %substitute#\[:VIM_EVAL:\]FILENAME\[:END_EVAL:\]#\=expand("%:r")#ge
augroup END
