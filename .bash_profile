#deluged &
export BROWSER="firefox" &
alsactl restore &
/etc/rc.d/mpd restart &
/etc/rc.d/mpdscribble restart &
if [[ -z "$DISPLAY" ]] && [[ $(tty) = /dev/tty1 ]]; then
  startx -- -nolisten tcp -dpi 96 &
fi
