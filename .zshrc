export TERM=xterm-256color
export HISTFILE=~/.zsh_history
export HISTSIZE=10000
export SAVEHIST=10000
eval `dircolors -b`

autoload -U compinit promptinit
compinit
promptinit
setopt ALWAYS_TO_END AUTO_LIST NO_LIST_BEEP SH_WORD_SPLIT
setopt NOMATCH BAD_PATTERN
setopt localoptions 
setopt NO_BEEP CD_ABLE_VARS 
setopt CORRECT APPEND_HISTORY SHARE_HISTORY HIST_IGNORE_DUPS
#WORDCHARS='*?_-.[]~\!#$%^(){}<>|`@#$%^*()+:?'
WORDCHARS='*?_.[]~\!#$%^()<>|`@#$%^*+:?'


#zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:*:kill:*' command 'ps -au$USER -o pid,%cpu,cmd'
zstyle ':completion:*' completer _expand _complete _correct
zstyle ':completion:*' format '%d:'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' matcher-list 'r:|[._-]=* r:|=*'
#zstyle ':completion:*' max-errors 3
zstyle ':completion:*' menu select=2 yes=long-list
zstyle ':completion:*' prompt 'Alternatives %e:'
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*:cd:*' ignore-parents parent pwd
#zstyle ':completion::*:(rm|vim|cp|mv):*' ignore-line true

autoload -U colors
colors
highlights='${PREFIX:+=(#bi)($PREFIX:t)(?)*==$color[cyan]=$color[none];$color[none]}':${(s.:.)LS_COLORS}}

zstyle -e ':completion:*' list-colors 'reply=( "'$highlights'" )'
unset highlights

# use colors when browsing man pages (if not using pinfo or PAGER=most)
[ -d ~/.terminfo/ ] && alias man='TERMINFO=~/.terminfo/ LESS=C TERM=mostlike PAGER=less man'

# key bindings
bindkey -e
bindkey "\e[1~" beginning-of-line
bindkey "\e[4~" end-of-line
bindkey "\e[5~" beginning-of-history
bindkey "\e[6~" end-of-history
bindkey "\e[3~" delete-char
bindkey "\e[2~" quoted-insert
bindkey "\e[5C" forward-word
bindkey "\eOc" emacs-forward-word
bindkey "\e[5D" backward-word
bindkey "\eOd" emacs-backward-word
bindkey "\e\e[C" forward-word
bindkey "\e\e[D" backward-word
#bindkey "^W" backward-delete-word
bindkey "^?"    backward-delete-char
bindkey "^H"    backward-delete-char
## for rxvt
bindkey "\e[8~" end-of-line
bindkey "\e[7~" beginning-of-line
## for non RH/Debian xterm, can't hurt for RH/DEbian xterm
bindkey "\eOH" beginning-of-line
bindkey "\eOF" end-of-line
## for freebsd console
bindkey "\e[H" beginning-of-line
bindkey "\e[F" end-of-line
## completion in the middle of a line
bindkey '^i' expand-or-complete-prefix

bindkey "^A" beginning-of-line
bindkey "^E" end-of-line
bindkey "^b" backward-word
bindkey "^f" forward-word
bindkey "^k" kill-line
bindkey ' ' magic-space    # also do history expansion on space
# bindkey '^I' complete-word # complete on tab, leave expansion to _expand


#FUNCTIONS

#find out ATI card temperature and set fan speed
function teplota { LD_PRELOAD=libXinerama.so.1 aticonfig --od-gettemperature; }
function fan() { aticonfig --pplib-cmd "set fanspeed 0 $1"; }

function ext () {
    if [[ "$#" == "1" ]]; then
        if [[ -f $1 ]]; then
            case $1 in
                *.tar.bz2)   tar xvjf $1    ;;
                *.tar.gz)    tar xvzf $1    ;;
                *.bz2)       bunzip2 $1     ;;
                *.rar)       unrar x $1     ;;
                *.gz)        gunzip $1      ;;
                *.tar)       tar xvf $1     ;;
                *.tbz2)      tar xvjf $1    ;;
                *.tgz)       tar xvzf $1    ;;
                *.zip)       unzip $1       ;;
                *.Z)         uncompress $1  ;;
                *.7z)        7z x $1        ;;
                *)           echo "don't know how to extract '$1'..." ;;
            esac
        else
            echo "'$1' is not a valid file!"
        fi
    else
        echo "Must provide only one argument!"
    fi
}

alias ls="ls --color -h"
alias pacman="pacman2"
alias cal="cal -m"
alias du="du -hs"
alias df="df -h"
alias grep="grep --colour -i"
alias rubysearch="ruby ~/skripty/search_interface.rb"
alias vi="vim -u ~/.vimrc"
alias xterm="LANG=sk_SK.utf8 xterm"
#alias more="vim -u /usr/share/vim/vim72/macros/less.vim"
#alias less="vim -u /usr/share/vim/vim72/macros/less.vim"
alias rtorrent="rtorrent -o http_capath=/etc/ssl/certs"
export m="/media/data/"
export M="/media/data2/"
#alias "true-combat"="fan 50; true-combat.alsa; fan 3;"
#alias soffice="env GTK2_RC_FILES=/usr/share/themes/Redmond/gtk-2.0/gtkrc env OOO_FORCE_DESKTOP=gnome soffice"
alias t="vim -c :TodoOpen -c :q -c ':noremap q :q<CR>'"
alias toclip="xclip -selection clipboard"
alias fromclip="xclip -selection clipboard -o"
alias newsbeuter="TERM=xterm newsbeuter"
alias pidgin="GTK2_RC_FILES=~/.themes/CrazyTrain/gtk-2.0/gtkrc pidgin"
alias firefox="GTK2_RC_FILES=~/.themes/CrazyTrain/gtk-2.0/gtkrc firefox"
alias smplayer="GTK2_RC_FILES=~/.themes/CrazyTrain/gtk-2.0/gtkrc smplayer 2>/dev/null"


autoload zsh/terminfo
#for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
for color in RED GREEN; do
    eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
    #eval PR__$color='%{$fg[${(L)color}]%}'
#    (( count = $count + 1 ))
done
PR_NO_COLOUR="%{$terminfo[sgr0]%}"

if [ `id -u` -eq 0 ]
then
    PS1="[${PR_RED} %/ $PR_NO_COLOUR]═> "
else
    PS1="[${PR_GREEN} %/ ${PR_NO_COLOUR}]═> "
fi
