-- Standard awesome library
require("awful")
require("awful.autofocus")
require("awful.rules")
-- Theme handling library
require("beautiful")
-- Notification library
--require("naughty")
require ("vicious")
require("scratchpad")



cardid  = 0
channel = "Master"
function volume (mode, widget)
    if mode == "update" then
        local fd = io.popen("amixer -c " .. cardid .. " -- sget " .. channel)
        local status = fd:read("*all")
        fd:close()

        local volume = string.match(status, "(%d?%d?%d)%%")
        volume = string.format("% 3d", volume)

        status = string.match(status, "%[(o[^%]]*)%]")

        --if string.find(status, "on", 1, true) then
            volume = '<span color="white">\tVolume:</span>'.. " [" .. volume .. " % ]"
        --else
        --  volume = '<span color="white">\tVolume:</span>'.. " [" .. " 0" .. " % ]"
        --end
        widget.text = volume
    elseif mode == "up" then
        io.popen("amixer -q -c " .. cardid .. " sset " .. channel .. " 2%+"):read("*all")
        volume("update", widget)
    elseif mode == "down" then
        io.popen("amixer -q -c " .. cardid .. " sset " .. channel .. " 2%-"):read("*all")
        volume("update", widget)
    else
        io.popen("amixer -c " .. cardid .. " sset " .. channel .. " toggle"):read("*all")
        volume("update", widget)
    end
end


-- {{{ Variable definitions
-- Themes define colours, icons, and wallpapers
beautiful.init("/home/cinan/.config/awesome/theme.lua")

-- This is used later as the default terminal and editor to run.
terminal = "terminal"
editor = os.getenv("EDITOR") or "vim"
editor_cmd = terminal .. " -e " .. editor


-- Default modkey.
-- Usually, Mod4 is the key with a logo between Control and Alt.
-- If you do not like this or do not have such a key,
-- I suggest you to remap Mod4 to another key using xmodmap or other tools.
-- However, you can use another modifier like Mod1, but it may interact with others.
modkey = "Mod4"

-- Table of layouts to cover with awful.layout.inc, order matters.
layouts =
{
    awful.layout.suit.tile, --1
    --awful.layout.suit.tile.left,  --
    --awful.layout.suit.tile.bottom, --
    --awful.layout.suit.tile.top, --
    --awful.layout.suit.fair,  --
    awful.layout.suit.fair.horizontal,  --2
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.max, --
    --awful.layout.suit.max.fullscreen, --
    awful.layout.suit.magnifier, --3
    --awful.layout.suit.floating  --4
}

-- }}}

-- {{{ Tags
-- Define a tag table which hold all screen tags.
tags = {}
--for s = 1, screen.count() do
    -- Each screen has its own tag table.
    tags[1] = awful.tag({ "www", "term", "mail", "chat", "code", "pdf"}, 1)
    awful.tag.setmwfact (0.75, tags[1][1])
    awful.tag.setmwfact (0.65, tags[1][3])
    for b = 1, 6 do
        awful.layout.set(layouts[1], tags[1][b])
    end
    awful.layout.set(layouts[4], tags[1][4])
--end
-- }}}
-- {{{ Menu
-- Create a laucher widget and a main menu
mymainmenu = awful.menu.new({ items = { --{ "awesome", myawesomemenu, beautiful.awesome_icon },
                                        --{ "Smplayer", "smplayer" },
                                        --{ "Gajim", "gajim" },
                                        --{ "Pidgin", "pidgin" },
                                        --{ "Dolphin", "dolphin" },
                                        { "OpenOffice", "soffice" },
                                        { "Gimp", "gimp" },
                                        --{ "Xsane", "xsane" },
                                        { "VirtualBox", "VirtualBox" },
                                        --{ "Lxappearance", "lxappearance" },
                                      }
                            })

-- }}}

-- {{{ Wibox

-- Create a wibox for each screen and add it
mywibox = {}
mypromptbox = {}
--mylayoutbox = {}
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
                    awful.button({ }, 1, awful.tag.viewonly),
                    awful.button({ modkey }, 1, awful.client.movetotag),
                    awful.button({ }, 3, awful.tag.viewtoggle),
                    awful.button({ modkey }, 3, awful.client.toggletag),
                    awful.button({ }, 4, awful.tag.viewnext),
                    awful.button({ }, 5, awful.tag.viewprev)
                    )
tb_volume = widget({ type = "textbox", name = "tb_volume", align = "right" })
tb_volume:buttons(awful.util.table.join(
    awful.button({ }, 4, function () volume("up", tb_volume) end),
    awful.button({ }, 5, function () volume("down", tb_volume) end),
    awful.button({ }, 1, function () volume("mute", tb_volume) end)
))
volume("update", tb_volume)

memwidget = widget({type = 'textbox',name = 'memwidget',align = "left"})
vicious.register(memwidget, vicious.widgets.mem, ' <span color="white">\tMemory:</span> [ $2M / $3M ]', 2)
--
cpuwidget = widget({ type = 'textbox', name = 'cpuwidget', align = 'left' })
vicious.register(cpuwidget, vicious.widgets.cpu, ' <span color="white">\tCPU:</span> [ $2 % <span color="white">|</span> $3 % ]', 2)
--
mpdwidget = widget({ type = 'textbox', name = 'mpdwidget',align = 'right'})
vicious.register(mpdwidget, vicious.widgets.mpd, '<span color="white">Now Playing:</span> [ $1 ]', 5)
--
fswidget = widget({ type = 'textbox', name = 'fswidget', align = 'left' })
vicious.register(fswidget, vicious.widgets.fs,
' <span color="white">\tFS:</span> [ ${/ avail_gb}G / ${/ size_gb}G <span color="white">|</span> ${/home avail_gb}G / ${/home size_gb}G <span color="white">|</span> ${/media/data avail_gb}G / ${/media/data size_gb}G <span color="white">|</span> ${/media/data2 avail_gb}G / ${/media/data2 size_gb}G ]'
, 120)
--
netwidget = widget({type = 'textbox', name = 'netwidget', align = 'left' })
vicious.register(netwidget, vicious.widgets.net, 
' <span color="white">\tNET:</span> [ ${eth0 down_kb} K/s / ${eth0 up_kb} K/s ]', 3)
--
datewidget = widget({  type = 'textbox', name = 'datewidget', align = 'right' })
vicious.register(datewidget, vicious.widgets.date, '\t[ <span color="white">%H:%M</span> %d.%m.%y]', 10)
--

for s = 1, screen.count() do
    -- Create a promptbox for each screen
    mypromptbox[s] = awful.widget.prompt({ layout = awful.widget.layout.horizontal.leftright })
    -- Create a taglist widget
    mytaglist[s] = awful.widget.taglist(s, awful.widget.taglist.label.all, mytaglist.buttons)

    -- Create a tasklist widget
    --mytasklist[s] = awful.widget.tasklist(function(c)
                                              --return awful.widget.tasklist.label.currenttags(c, s)
                                          --end, mytasklist.buttons)

    -- Create the wibox
    mywibox[s] = awful.wibox({ position = "top", screen = s })
    -- Add widgets to the wibox - order matters
    mywibox[s].widgets = {
            {mytaglist[s],
            mypromptbox[s],
            cpuwidget,
            memwidget,
            fswidget,
            netwidget,
            layout = awful.widget.layout.horizontal.leftright},
            datewidget,
            tb_volume,
            mpdwidget,
            layout = awful.widget.layout.horizontal.rightleft
    }
end
-- }}}

-- {{{ Mouse bindings
root.buttons(awful.util.table.join(
    awful.button({ }, 2, function () awful.menu.clients(mymainmenu) end),
    awful.button({ }, 3, function () mymainmenu:toggle() end),
    awful.button({ }, 4, awful.tag.viewnext),
    awful.button({ }, 5, awful.tag.viewprev)
))
-- }}}


-- {{{ Key bindings
globalkeys = awful.util.table.join(
    awful.key({ modkey },            "s", function () scratchpad.toggle() end),

    awful.key({ modkey,           }, "Left",   awful.tag.viewprev       ),
    awful.key({ modkey,           }, "Right",  awful.tag.viewnext       ),
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "j",
        function ()
            awful.client.focus.byidx( 1)
            if client.focus then client.focus:raise() end
        end),
    awful.key({ modkey,           }, "k",
        function ()
            awful.client.focus.byidx(-1)
            if client.focus then client.focus:raise() end
        end),
    --awful.key({ modkey,           }, "w", function () mymainmenu:show(true)        end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1) end),
    awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1) end),
    awful.key({ modkey, "Control" }, "j", function () awful.screen.focus( 1)       end),
    awful.key({ modkey, "Control" }, "k", function () awful.screen.focus(-1)       end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
--    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(layouts,  1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(layouts, -1) end),

    awful.key({ modkey,           }, "F1", function () awful.util.spawn("/home/cinan/skripty/firefox.sh") end),
    --awful.key({ modkey,           }, "F2", function () awful.util.spawn("thunderbird") end),
    awful.key({ modkey,           }, "F2", function () awful.util.spawn("/home/cinan/skripty/hudba.sh") end),
    awful.key({ modkey,           }, "F3", function () awful.util.spawn("/home/cinan/skripty/pidgin.sh") end),
    awful.key({ modkey,           }, "F4", function () awful.util.spawn("smplayer") end),
    awful.key({ modkey,           }, "z", function () awful.util.spawn("mpc prev") end),
    awful.key({ modkey,           }, "x", function () awful.util.spawn("mpc play") end),
    awful.key({ modkey,           }, "c", function () awful.util.spawn("mpc toggle") end),
    awful.key({ modkey,           }, "v", function () awful.util.spawn("mpc stop") end),
    awful.key({ modkey,           }, "b", function () awful.util.spawn("mpc next") end),
    awful.key({ "Mod1",           }, "j", function () awful.util.spawn("amixer -q sset Master 2%-") end, function () volume("update", tb_volume) end),
    awful.key({ "Mod1",           }, "k", function () awful.util.spawn("amixer -q sset Master 2%+") end, function () volume("update", tb_volume) end),
    awful.key({ modkey, "Shift"   }, "q", function () awful.util.spawn("xkill") end),
    awful.key({ modkey,           }, "XF86PowerOff", function () awful.util.spawn("sudo halt") end),
    awful.key({ modkey,           }, "XF86Sleep", function () awful.util.spawn("sudo pm-suspend") end),
    awful.key({ modkey,           }, "XF86Standby", function () awful.util.spawn("sudo pm-suspend") end),
    awful.key({ modkey            }, "Print", function () awful.util.spawn("ksnapshot") end),
    
    -- Prompt
    awful.key({ "Mod1" },            "F2",     function () mypromptbox[mouse.screen]:run() end),
    awful.key({ "Mod1",           }, "F3", function () awful.util.spawn("gnome-terminal") end)
)

-- Client awful tagging: this is useful to tag some clients and then do stuff like move to tag on them
clientkeys = awful.util.table.join(
    awful.key({ modkey }, "d", function (c) scratchpad.set(c, 0.60, 0.60, true) end),
    awful.key({ modkey,           }, "f",      function (c) c.fullscreen = not c.fullscreen  end),
    awful.key({ "Mod1"            }, "F4",     function (c) c:kill()                         end),
    awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle                     ),
    awful.key({ modkey, "Control" }, "Return", function (c) c:swap(awful.client.getmaster()) end),
    awful.key({ modkey,           }, "o",      awful.client.movetoscreen                        ),
    awful.key({ modkey, "Shift"   }, "r",      function (c) c:redraw()                       end),
    awful.key({ modkey,}, "m",
        function (c)
            c.maximized_horizontal = not c.maximized_horizontal
            c.maximized_vertical   = not c.maximized_vertical
        end)
)

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
--for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[1], keynumber));
--end


-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                          local screen = mouse.screen
                          if tags[screen][i] then
                              awful.tag.viewonly(tags[screen][i])
                          end
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end

clientbuttons = awful.util.table.join(
    awful.button({ }, 1, function (c) client.focus = c; c:raise() end),
    awful.button({ modkey }, 1, awful.mouse.client.move),
    awful.button({ modkey }, 3, awful.mouse.client.resize))

-- Set keys
root.keys(globalkeys)
-- }}}

-- {{{ Rules
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    { rule = { class = "Smplayer" },
      properties = { floating = true } },
    { rule = { class = "File-roller" },
      properties = { floating = true } },
    { rule = { class = "Wine" },
      properties = { floating = true } },
    { rule = { class = "VirtualBox" },
      properties = { floating = true } },

    { rule = { role = "buddy_list" },
      properties = { floating = true } },
    { rule = { role = "conversation" },
      properties = { floating = true } },
    { rule = { role = "first" },
      properties = { tag = tags[1][1] } },
    { rule = { role = "third" },
      properties = { tag = tags[1][3] } },

    { rule = { instance = "Extension" },
      properties = { floating = true } },
    { rule = { instance = "Browser" },
      properties = { floating = true } },
    { rule = { instance = "Places" },
      properties = { floating = true } },
    { rule = { instance = "Toplevel" },
      properties = { floating = true } },


    --{ rule = { class = "Firefox" },
      --properties = { tag = tags[1][1] } },
    --{ rule = { class = "Gran Paradiso" },
      --properties = { tag = tags[1][1] } },
    --{ rule = { class = "Shiretoko" },
      --properties = { tag = tags[1][1] } },
    { rule = { class = "Pidgin" },
      properties = { tag = tags[1][4] } },
}
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.add_signal("manage", function (c, startup)
    -- Add a titlebar
    -- awful.titlebar.add(c, { modkey = modkey })

    -- Enable sloppy focus
    c:add_signal("mouse::enter", function(c)
        if awful.layout.get(c.screen) ~= awful.layout.suit.magnifier
            and awful.client.focus.filter(c) then
            client.focus = c
        end
    end)

    if not startup then
        -- Set the windows at the slave,
        -- i.e. put it at the end of others instead of setting it master.
        awful.client.setslave(c)
        if c.role == "buddy_list" then --pidgin buddy list
            c:geometry({ x = 0, y = 15, width = 177, height = 1035 })
        end

        -- Put windows in a smart way, only if they does not set an initial position.
        if not c.size_hints.user_position and not c.size_hints.program_position then
            --awful.placement.no_overlap(c)
            --awful.placement.no_offscreen(c)
            awful.placement.centered(c)
        end
    end
end)

client.add_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.add_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

awful.util.spawn("/home/cinan/skripty/autostart.sh")
--awful.util.spawn("run_once pidgin")
awful.util.spawn("/home/cinan/skripty/pidgin.sh")
awful.util.spawn("run_once parcellite -n")
--awful.util.spawn("run_once hudba &>/dev/null")
awful.util.spawn("run_once terminal --role=third -x newsbeuter")
awful.util.spawn("run_once terminal --role=third -x mutt")
awful.util.spawn("xcompmgr -c -l 0 -t 0 -o .50 -r 2 -C")
